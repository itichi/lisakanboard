window.$ = window.jQuery = require('jquery');

let content = document.getElementById("content");

let maximizeStatus = false;

const webview = document.querySelector('webview');

const remote = require('electron').remote;

// http://stackoverflow.com/questions/37429186/how-to-imitate-trello-board-click-drag-to-scroll
// https://jsfiddle.net/ro2mtm98/3/
const dragBackground = `
let curYPos = 0;
let curXPos = 0;
let curDown = false;
let boardContainer = $('#board-container')[0];

$('table#board')[0].addEventListener('mousemove', function(e){ 
  if(curDown === true){
    window.scrollTo(1000, document.body.scrollTop + (curYPos - e.pageY));
    boardContainer.scrollLeft = (curXPos - e.pageX);
  }
});

$('table#board')[0].addEventListener('mousedown', function(e) {
    let $target = $(e.target);
    curDown = true;
    curXPos = e.pageX + boardContainer.scrollLeft;
    curYPos = e.pageY;
});
$('table#board')[0].addEventListener('mouseup', function(e){
    curDown = false;
}); 

$(function() {
  for (var i = 1 ; i <= 8 ; i++) {
    $( "#list" + i ).sortable({
      connectWith: "#list1, #list2, #list3, #list4, #list5, #list6, #list7, #list8"
    }).disableSelection();
  }
});
`

function quitMe() {
  var window = remote.getCurrentWindow();
  window.close();
}

function minimize(){
  var window = remote.getCurrentWindow();
  window.minimize();  
}

function backMe(){
  if (webview.canGoBack()){
    webview.goBack();
  }
}

function forwardMe(){
  if (webview.canGoForward()){
    webview.goForward();
  }
}

function maximizeMe(){
  var window = remote.getCurrentWindow();
  
  if (!maximizeStatus) {
	  window.maximize();
  }
  else {
	  window.unmaximize();
  }
  maximizeStatus = !maximizeStatus;
}

document.addEventListener("keydown", event => {
  switch (event.key) {
      case "Escape":
          var window = remote.getCurrentWindow();
          window.close();
          break;
       }
});

webview.addEventListener('dom-ready', () => {
	webview.openDevTools();
	webview.executeJavaScript(dragBackground);
})

/*
$( document ).ready(function() {
	// http://stackoverflow.com/questions/676705/changing-data-content-on-an-object-tag-in-html
	let elem = document.getElementById("embedContent");
	
	let columns = document.getElementsByClassName("board-column-170");
	var i;
	for (i = 0; i < columns.length; i++) {
		columns[i].style["-webkit-app-region"] = "drag";
	}
	
	let x = document.getElementById("board").tBodies;
	console.log(x);
	var j;
	for (j = 0; j < x.length; j++) {
		x[j].style["-webkit-app-region"] = "drag";
	}
	
})
//*/
